<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Image
 *
 * @author Luciana
 */
class Image {

    private $id;
    private $letter;
    private $word;
    private $audio;
    private $image;

    function __construct($id, $letter, $word, $audio, $image) {
        $this->id = $id;
        $this->letter = $letter;
        $this->word = $word;
        $this->audio = $audio;
        $this->image = $image;
    }

    public function getId() {
        return $this->id;
    }

    public function getLetter() {
        return $this->letter;
    }

    public function getWord() {
        return $this->word;
    }

    public function getAudio() {
        return $this->audio;
    }

    public function getImage() {
        return $this->image;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setLetter($letter) {
        $this->letter = $letter;
    }

    public function setWord($word) {
        $this->word = $word;
    }

    public function setAudio($audio) {
        $this->audio = $audio;
    }

    public function setImage($image) {
        $this->image = $image;
    }

}

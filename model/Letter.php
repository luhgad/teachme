<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of letter
 *
 * @author Luciana
 */
class Letter {

    private $id;
    private $letter;
    private $audio;

    function __construct() {
        
    }

    public function getId() {
        return $this->id;
    }

    public function getLetter() {
        return $this->letter;
    }

    public function getAudio() {
        return $this->audio;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setLetter($letter) {
        $this->letter = $letter;
    }

    public function setAudio($audio) {
        $this->audio = $audio;
    }

}

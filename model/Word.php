<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of word
 *
 * @author Luciana
 */
class Word {
    private $id;
    private $word;
    private $audio;
    private $letter;
    
    function __construct($id, $word, $audio, $letter) {
        $this->id = $id;
        $this->word = $word;
        $this->audio = $audio;
        $this->letter = $letter;
    }

    public function getId() {
        return $this->id;
    }

    public function getWord() {
        return $this->word;
    }

    public function getAudio() {
        return $this->audio;
    }

    public function getLetter() {
        return $this->letter;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setWord($word) {
        $this->word = $word;
    }

    public function setAudio($audio) {
        $this->audio = $audio;
    }

    public function setLetter($letter) {
        $this->letter = $letter;
    }
}
